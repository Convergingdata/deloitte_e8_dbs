
// Translations for en_US
i18n_register({"plural": function(n) { return n == 1 ? 0 : 1; }, "catalog": {}});


 require([
    'underscore',
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/dataview',
	"splunkjs/mvc/searchmanager",
    'splunkjs/mvc/simplexml/ready!'
], function(_, $, mvc, DataView,SearchManager) {
	console.log("here");
	var templateString = "\
<%\
for(var i=0, l=results.length; i<l; i++) { \
	var field=results[i]; %> \
	<div class=\"question_root_container\" data-questionnaire-id=\"<%= field['questionnaireId'] %>\" data-question-number=\"<%= field['number'] %>\" >\
	<div class=\"question_option_table\">\
		<div class=\"question_header\">\
			<span class=\"question_number\"><%= field['number'] %></span>\
			<span class=\"question_title\"><%= field['question'] %></span>\
		</div>\
	</div>\
	<div class=\"question_narrative\"><%= field['narrative'] %></div>\
	<div class=\"question_option_table has_comment_<%= field['hasComment']%>\">\
		<div class=\"question_option_row\">\
			<div class=\"question_options\" data-question-number=\"<%= field['number'] %>\">\
				<label class=\"option_container\" data-value=\"<%= field['option1'] %>\"><%= field['option1'] %><input type=\"radio\" name=\"q_<%= field['number'] %>\" data-option-number=\"1\" value=\"<%= field['option1'] %>\"><span class=\"checkmark\"></span></label>\
				<label class=\"option_container\" data-value=\"<%= field['option2'] %>\"><%= field['option2'] %><input type=\"radio\" name=\"q_<%= field['number'] %>\" data-option-number=\"2\" value=\"<%= field['option2'] %>\"><span class=\"checkmark\"></span></label>\
				<label class=\"option_container\" data-value=\"<%= field['option3'] %>\"><%= field['option3'] %><input type=\"radio\" name=\"q_<%= field['number'] %>\" data-option-number=\"3\" value=\"<%= field['option3'] %>\"><span class=\"checkmark\"></span></label>\
			</div>\
			<div class=\"question_comment data-questionnaire-id=\"<%= field['questionnaireId'] %>\" data-question-number=\"<%= field['number'] %>\">\
				<textarea id=\"question_<%= field['number'] %>_comment\" placeholder=\"Comments...\"></textarea>\
			</div>\
		</div>\
	</div>\
</div>\
<% }%> \
";

	var questionnaire = new DataView({
		id: "dv_questionnaire",
		managerid: "questionnaire",
		template: templateString,
		el: $("#questionnaire")
	}).render();
		
	$("#cmd_submit").on("click",function(){
		console.log("Saving Questionnaire");
		var tokens = mvc.Components.get('env');
		var defaultTokens = mvc.Components.get('default');
		var spl = "";
		var questionnaire_id = "";
		var question_number = 0;
		var user = tokens.get("user");
		var email = tokens.get("user_email");
		var app =  tokens.get("app");
		var questionnaire_id = "";
		var user_realname = tokens.get("user_realname");
		var _time = Math.floor(Date.now() / 1000);
		// Validate: Only continue if all are answered.
		if ( ! validate_questionnaire()){
			return false;
		}
		// Disable the button so we don't click twice
		$("#cmd_submit").attr('disabled', true);
		
		/*
		 * Iterate through the answers and build up the responses in a token 
		*/
		$('.question_root_container').each(function () {
			var q_number = $(this).attr("data-question-number");
			questionnaire_id = $(this).attr("data-questionnaire-id");
			var q_text = $(this).find("span.question_title").first().text();
			var response_key = $(this).find('input:checked').first().attr("data-option-number");
			var response_value  = $(this).find('input:checked').first().val();
			var comment = $(this).find("div.has_comment_true textarea").first().val() || "";
			spl += " _time=" + _time +", q='" + q_number + "', question_text='" + sanitize(q_text) + "', qid='" + questionnaire_id +  "', ";
			spl += response_key = "response_key='" + sanitize(response_key) + "', response_value='" + sanitize(response_value) + "'";
			spl += (comment!="") ?  ", comment='" + sanitize(comment) + "'" : "";
			spl += "####";
		});

		// Save the raw event into a token:
		defaultTokens.set("_raw", spl.replace(/\[/g,"\\[").replace(/\]/g,"\\]"));
		var application_name = defaultTokens.get("application_name");
		// Run saved search to save responses
		var saveResponses = new SearchManager({
				"id": "e8_questionnaire_collect",
				"search": "|savedsearch e8_questionnaire_collect raw=\"$_raw$\"",
				"latest_time": "-1m",
				"earliest_time": "-1h",
				"cancelOnUnload": false,
				"auto_cancel": 90
			},{tokens:true});
	
		saveResponses.on("search:done", function(properties) {
			console.log("Responses successfully saved.\nSearch job properties:", properties);
			var tokens = mvc.Components.get('default');
			var redirect_on_success= tokens.get("redirect_on_success") || "anz_maturity_assessment_overview_phase_2";
			var newhref=window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/app/" + app + "/" + redirect_on_success;
			$("div#questionnaire").html("<h1>Success</h1><p>Thank you for submitting your responses.</p><p>You will be redirected shortly.<br/><a href=\"" + newhref +"\" title=\"Proceed Immediately. \">Proceed immediately</a></p>");
			$("#cmd_submit").remove();
			$("#error_message").remove();
			$("#note").remove();
			setTimeout(function(){window.location.replace(newhref);},3000);
		});
		
		// What happens on search error:
		saveResponses.on("search:failed search:error", function(properties) {
			console.log("Error\nSearch job properties:", properties);
			$("div#questionnaire").html("<h1>Submission Failed</h1><p>Your response was not successfully saved. Please refresh the page and try again.</p>");
			$("#cmd_submit").remove();
			$("#error_message").remove();
		});
		
		return false; // This is here so the submit button won't refresh the page.
	});

	/* 
	 * Sanitize a String so it can be used in | eval field=XXXXX
	 * Removes: ", '
	 * Escapes: \\, \n, \r, '
	 * Replaces: ] => \], [ => \[
	 */
	function sanitize(str){
		return str.replace(/\\/g,"\\\\").replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\[/g,"\\[").replace(/\]/g,"\\]").replace(/"/g, " ").replace(/'/g, "\\'");
	}

	/*
	 * Check all options div to make sure there's at least one checked.
	 * Returns:
	 *     true if all answered
	 *     false if some unanswered
	 */
	function validate_questionnaire(){
		var html = "";
		$(".question_number, .question_title").removeClass("error");
		$('.question_root_container').each(function () {
			//find span element within div.question with class 'label' and get the text
			if (!$(this).find('input').is(':checked')) {
				// Didn't validate ... display alert or do something
				$(this).find(".question_number, .question_title").addClass("error");
				html +="<li class=\"error\">Question " + $(this).find(".question_options").attr("data-question-number") + "</li>";
			}
		});
		if(html==""){
			return true;
		}else{
			$("div#error_message").html("<span>Plese answer the following questions:</span><ul>" + html + "</ul>");
			return false;
		}
	}
	
	/*
	 * Un-error-highlight questions as we answer them
	 */
	 $("body").on("click", "input[type='radio']", function(){
		console.log("updated radio");
		var parentObj = $(this).parents("div.question_root_container");
		var qnumber = $(parentObj).attr("data-question-number");
		parentObj.find("span.error").removeClass("error");
		$("div#error_message li.error").each( function(){
			if($(this).text()=="Question " + qnumber){
				$(this).remove();
			}
		});
		if( $("div#error_message li.error").length ==0){
			$("div#error_message").html(""); 
		}
		
	 });
	
	
	/* Display partial text */
	var defaultTokens = mvc.Components.get('url');	
	if(defaultTokens.get("partial")){
		$("div#partial").show();
	}
	
	
});

